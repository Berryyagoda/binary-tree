import java.util.Collection;
import java.util.Iterator;

public class BinaryTree implements Collection<Integer> {

    private Node root;
    private int size = 0;
    private int index = 0;

    @Override
    public Iterator<Integer> iterator() {
        return new Iterator<Integer>() {

            final Integer[] values = toArray();
            private int index = 0;

            @Override
            public boolean hasNext() {
                return index < values.length;
            }

            @Override
            public Integer next() {
                int oldIndex = index;
                index++;
                return values[oldIndex];
            }

        };
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        return containsNodeRecursive(root, (int) o);
    }

    @Override
    public void clear() {
        size = 0;
        root = null;
    }

    @Override
    public Integer[] toArray() {
        Integer[] values = new Integer[size];
        index = 0;
        traverseInOrder(root, values);
        return values;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean add(Integer integer) {
        if(contains(integer)) return false;
        root = addRecursive(root, integer);
        return true;
    }

    @Override
    public boolean remove(Object o) {
        if(contains(o)) {
            root = deleteRecursive(root, (int) o);
            return true;
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if (!contains(o)) return false;
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends Integer> c) {
        for(Integer o : c)
            add(o);
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        for(Object o : c)
            remove(o);
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    private void traverseInOrder(Node node, Integer[] values) {
        if (node != null) {
            traverseInOrder(node.left, values);
            values[index] = node.value;
            index++;
            traverseInOrder(node.right, values);
        }
    }

    private Node addRecursive(Node current, int value) {
        if (current == null) {
            size++;
            return new Node(value);
        }
        if (value < current.value) {
            current.left = addRecursive(current.left, value);
        } else if (value > current.value) {
            current.right = addRecursive(current.right, value);
        } else {
            return current;
        }
        return current;
    }

    private boolean containsNodeRecursive(Node current, int value) {
        if (current == null) {
            return false;
        }
        if (value == current.value) {
            return true;
        }
        return value < current.value
                ? containsNodeRecursive(current.left, value)
                : containsNodeRecursive(current.right, value);
    }

    private Node deleteRecursive(Node current, int value) {
        if (current == null) {
            return null;
        }
        if (value == current.value) {
            if (current.left == null && current.right == null) {
                size--;
                return null;
            }
            if (current.right == null) {
                return current.left;
            }
            if (current.left == null) {
                return current.right;
            }
            int smallestValue = findSmallestValue(current.right);
            current.value = smallestValue;
            current.right = deleteRecursive(current.right, smallestValue);
            return current;
        }
        if (value < current.value) {
            current.left = deleteRecursive(current.left, value);
            return current;
        }
        current.right = deleteRecursive(current.right, value);
        return current;
    }

    private int findSmallestValue(Node root) {
        return root.left == null ? root.value : findSmallestValue(root.left);
    }

}
