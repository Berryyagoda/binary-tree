import java.util.Collection;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.TreeSet;

public class Application {

    public static void main(String[] args) {
        Collection<Integer> tree = new BinaryTree();
        tree.add(10);
        tree.add(15);
        tree.add(20);
        tree.add(11);
        tree.add(9);
        System.out.println("Tree contains 11: " + tree.contains(11));
        System.out.println("Tree size: " + tree.size());
        printTree(tree);
        tree.remove(20);
        tree.remove(9);
        System.out.println("New tree size: " + tree.size());
        printTree(tree);
        tree.clear();
        System.out.println("Tree size after clear: " + tree.size());
        printTree(tree);
    }

    private static void printTree(Collection<Integer> tree) {
        Iterator<Integer> treeIterator = tree.iterator();
        System.out.print("Values: ");
        while(treeIterator.hasNext())
            System.out.print(" " + treeIterator.next());
        System.out.println();
    }

}
